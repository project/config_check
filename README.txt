# Config Check

The Config Check module provides a report that you can run just before you launch a
site to make sure that you've got all your Drupal ducks in a row. Among other
things, it checks that:

- Caching and JS / CSS aggregation are turned on
- You have pathauto installed with best practice settings
- You have path_redirect installed with best practice settings
- Cron has been run recently
- You've got spam protection on your webforms
- Submission addresses of your webforms don't contain any internal or testing
  domains
- You don't have any published nodes that contain "dummy" text, like Lorem Ipsum
- You've installed a WYSIWYG and assigned it to at least one other input format
- You have at least one user with an administrator role

It has configurable settings and can be extended to add your own custom reports.

## Installation

Config Check is installed just like any other module; to wit:

1. Extract all the files to your sites/all/modules folder.
2. Enable the module at Administer -> Modules.
3. Make sure you have the 'access site reports' permission.
4. Run the report at Administer -> Reports -> Config Check.

## Configuration

You can find the configuration settings at
Administer -> Site Configuration -> Config Check.

The Config Check module has the following configuration settings:

* Specify what should count as dummy text (lorem, ipsum, blahblahblah, etc.)
* Choose whether you want to check unpublished nodes for dummy text
* Add the domains that should count as dummy / internal testing domains for
  webform emails
* Enable and disable the various reports

## Extending the Config Check Module

Adding your own reports to the Config Check module is pretty easy. Essentially,
there are two steps:

1. Register your report.
2. Write your report, making sure it returns the data that Config Check expects

### Register Your Report

To register your report, use the `hook_config_check_report_register()` function.
Your function should return an array of data about the reports you want to
register. For example, if your custom module is called "My Config Check Reports",
you would include a function like this in your module:

    function my_config_check_reports_config_check_report_register() {
      $report[] = array(
        'title' => 'The Name of My Report',
        'function_name' => 'the_function_that_runs_my_report',
      );
      return $report;
    }

That's all there is to it, but for another example, see
`config_check_config_check_report_register()` in config_check.module.

### Write Your Report

Your report can do anything you want it to, so long as it returns it returns an
array named $issues. The $issues array must contain a list of problems that your
report found (e.g., a module is not installed, a module is not configured
correctly, etc.).

Each problem should be a simple text-based message suitable for being displayed
on the Config Check report page. Wrap your message in the t() function so that it
may be translated. If you don't find any problems, just return an empty array.

Here's a sample function:

function my_config_check_reports_check_something() {
  $issues = array();

  /** check stuff **/
  /** beep beep boop boop checking beeep beep **/
  /** PROBLEM FOUND!!! **/

  $issues[] = t('Your site will explode in 5 minutes if you don't fix this.');

  return $issues();
}

See config_check.module for plenty more examples of what a report can do or should
look like.

## Support

Issue queue: TBD
