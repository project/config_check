<?php

/**
 * @file
 * Testing module to report site's implementation of development standards and best practices
 *
 * Additional tests can be added by other modules that implement the
 * config_check_report_register hook.
 *
 * The core report is provided in this module by config_check_config_check_report,
 * which in turn delegates each individual functionality report to a helper
 * function.
 */

/**
 * Configuration settings.
 */
function config_check_admin_settings() {
  $form['config_check_report_url'] = array(
    '#value' => '<a href="/admin/reports/config_check">Go to the Config Check reports page</a>',
  );
  $form['dummy_text']['config_check_dummy_text'] = array(
    '#type' => 'textarea',
    '#cols' => 2,
    '#title' => t('Dummy Text'),
    '#default_value' => variable_get('config_check_dummy_text', 'lorem|ipsum|dolor|amet'),
    '#description' => t('Enter a pipe-separated list of words that should count as dummy text. Example: lorem|ipsum'),
  );
  $form['dummy_text']['config_check_dummy_text_only_check_published'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only check published nodes for dummy text'),
    '#default_value' => variable_get('config_check_dummy_text_only_check_published', 1),
    '#description' => t('Clear this checkbox to check the body text of both published and unpublished nodes.'),
  );
  $form['config_check_internal_email_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Internal / Testing Email Domains'),
    '#default_value' => variable_get('config_check_internal_email_domain', 'example.com'),
    '#description' => t('Enter a pipe-separated list of internal email domains. The Webform report uses these domains to check for webforms that submit to internal or testing addresses. Example: example.com|mycompany.com'),
  );

  return system_settings_form($form);
}

/**
 * Enable or disable reports.
 */
function config_check_admin_enable_disable() {
  $registered_reports = config_check_get_registered_reports();
  $options = _config_check_get_checkboxes($registered_reports);

  $form['config_check_enabled_reports'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => _config_check_get_enabled_reports(),
    '#title' => t('Run the following reports'),
    '#description' => t('Select the reports that you want to run'),
  );

  $form['#submit'][] = '_config_check_form_submit';

  return system_settings_form($form);
}

/*
 * Helper function for config_check_admin_enable_disable().
 *
 * @param array $registered_reports
 *   A list of registered Config Check reports.
 *
 * @return array
 *   A list in the proper format to render checkboxes via the Drupal FAPI.
 */
function _config_check_get_checkboxes($registered_reports) {
  $options = array();
  foreach ($registered_reports as $report) {
    $options[$report->function_name] = $report->title;
  }
  return $options;
}

/**
 * Custom submit handler for the form provided by config_check_admin_enable_disable().
 *
 * Updates each report's status (enabled or disabled) in the database.
 * @return void
 */
function _config_check_form_submit($form, &$form_state) {
  foreach ($form_state['values']['config_check_enabled_reports'] as $function_name => $status) {
    $report = _config_check_get_report($function_name);
    if ($report) {
      if (strcmp($function_name, $status) == 0) {
        // enabled
        $report->status = 1;
      }
      else {
        $report->status = 0;
      }
      $result = drupal_write_record('config_check', $report, 'rid');
    }
  }
}

/**
 * Look up a report based on its function name.
 *
 * @param string $function_name
 *   The function name.
 * @return object
 *   Details on the report, or false.
 */
function _config_check_get_report($function_name) {
  return db_fetch_object(db_query("SELECT * FROM {config_check} WHERE function_name = '%s'", $function_name));
}
